function [label, spdata, pvdata, mycmd, mypac, myfft, meanFeat, stdFeat] = getFeatByTrial(vel, classind, fingerid, trialid, veld1, veld2, veld3, cmd1, cmd2, cmd3)
    cmddata = 0;
    if (vel == 1)
        data = veld1;
        cmddata = cmd1;
    elseif (vel == 2)
        data = veld2;
        cmddata = cmd2;
    elseif (vel == 3)
        data = veld3;
        cmddata = cmd3;
    end

    classdata = data{classind};
    cmddata = cmddata{classind}{2}{fingerid};

    label = classdata{1};
    classdata = classdata{2};
    pac0 = classdata{1};
    electr = classdata{4};
    posstates = classdata{5};
    % fingers
    posstates = posstates{fingerid};
    pac0 = pac0{fingerid};
    electr = electr{fingerid};

    sp = posstates{2};
    pv = posstates{3};

    uvals = unique(sp(size(sp, 2)/2-500:size(sp, 2)/2+500));
    secval = min(uvals);

    trialindex = 1;
    for i=1:trialid
        trstat = 0;
        for j=trialindex:numel(sp)
            if (sp(j) == secval || trialid == 10 && sp(j) < secval)
                if (trstat == 0)
                    % first entry
                    trstat = 1;
                end
                if (trstat == 2)
                    trialindex = j;
                    endpos = j - 1;
                    break;
                end
            else
                if (trstat == 1)
                    startpos = j;
                    trstat = 2;
                end
            end
        end
    end
 
    spdata = sp(startpos:endpos);
    pvdata = pv(startpos:endpos);
    mycmd  = cmddata(startpos:endpos);
    
    mypac = pac0((startpos)*10:(endpos)*10);  
    myfft = abs(fft(mypac(2:end)));
    
    myelectr = electr{startpos*10:endpos*10};
    
    meanFeat = {};
    stdFeat = {};
    for i=1:size(myelectr, 1)
        e1 = myelectr{i};
        meanFeat{i} = nanmean(e1);
        stdFeat{i} = nanstd(e1);
    end
end

